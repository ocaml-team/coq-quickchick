Source: coq-quickchick
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Julien Puydt <jpuydt@debian.org>
Section: ocaml
Priority: optional
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: coq,
               cppo,
               debhelper-compat (= 13),
               dh-coq,
               dh-ocaml,
               ocaml-dune,
               help2man,
               libcoq-ext-lib,
               libcoq-mathcomp-ssreflect,
               libcoq-core-ocaml-dev,
               libcoq-simple-io,
               libcoq-stdlib,
               menhir,
               ocamlbuild
Vcs-Browser: https://salsa.debian.org/ocaml-team/coq-quickchick
Vcs-Git: https://salsa.debian.org/ocaml-team/coq-quickchick.git
Homepage: https://github.com/Quickchick/Quickchick

Package: libcoq-quickchick
Architecture: any
Depends: ${coq:Depends}, ${misc:Depends}
Provides: ${coq:Provides}
Description: randomized testing framework for Coq (plugin)
 QuickChick provides a framework for randomized testing
 of program properties ; it's a clone of Haskell's
 QuickCheck.
 .
 It includes a foundational verification framework to test
 code and a mechanism to automatically derive generators
 for inductive relations.
 .
 Coq is a proof assistant for higher-order logic.
 .
 This package contains the Coq plugin.

Package: quickchick
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, ocaml-base | ocaml
Description: randomized testing framework for Coq (tools)
 QuickChick provides a framework for randomized testing
 of program properties ; it's a clone of Haskell's
 QuickCheck.
 .
 It includes a foundational verification framework to test
 code and a mechanism to automatically derive generators
 for inductive relations.
 .
 Coq is a proof assistant for higher-order logic.
 .
 This package contains the tools.
